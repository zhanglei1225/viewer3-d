package io.renren;

import io.renren.common.utils.RedisUtils;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static io.renren.common.utils.ShiroUtils.getUserId;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
	@Autowired
	private RedisUtils redisUtils;

	@Test
	public void contextLoads() {
		SysUserEntity user = new SysUserEntity();
		user.setEmail("qqq@qq.com");
		redisUtils.set("user", user);

		System.out.println(ToStringBuilder.reflectionToString(redisUtils.get("user", SysUserEntity.class)));
	}

	@Test
	public void testAmount(){
		Map<String, Object> map = new HashMap<>();
		map.put("codeUrl", "weixin://wxpay/bizpayurl?pr=p4lpSuKzz");
		map.put("number", "123456");
		redisUtils.set("1"+":"+20,map,2*3600);
		String obj = redisUtils.get("1" + ":" + 20);
		Map<String, Object> map1 = redisUtils.fromJson(obj, Map.class);
		System.out.println(map1);
	}
}
