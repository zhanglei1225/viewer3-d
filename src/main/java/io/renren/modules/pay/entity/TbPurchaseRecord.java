package io.renren.modules.pay.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * ClassName: TbRechargeRecord
 * Package: io.renren.modules.sys.entity
 * Description: 消费记录表
 *
 * @Author Tree
 * @Create 2023/12/7 10:44
 * @Version 11
 */
@Data
@TableName("tb_recharge_record")
public class TbPurchaseRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private String Id;

    private Long userId;

    /**
     * 消费金额
     */
    private Integer purchaseAmount;

    /**
     * 消费时间
     */
    private LocalDateTime purchaseTime;
}
