package io.renren.modules.pay.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * ClassName: TbPoints
 * Package: io.renren.modules.sys.entity
 * Description: 积分表
 *
 * @Author Tree
 * @Create 2023/12/7 10:40
 * @Version 11
 */
@Data
@TableName("tb_points")
public class TbPoints implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private String Id;

    private Long userId;

    /**
     * 积分数量
     */
    private Integer pointsAmount;

    /**
     * 首充时间
     */
    private LocalDateTime rechargeTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
