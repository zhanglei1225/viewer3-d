package io.renren.modules.pay.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * ClassName: TbRechargeRecord
 * Package: io.renren.modules.sys.entity
 * Description: 充值记录表
 *
 * @Author Tree
 * @Create 2023/12/7 10:44
 * @Version 11
 */
@Data
@TableName("tb_recharge_record")
public class TbRechargeRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 支付状态 0未支付 1已支付 2退款中 3 已退款 4退款失败
     */
    public static final Integer UN_PAID = 0;
    public static final Integer PAID = 1;
    public static final Integer REFUND_PROCESSING = 2;
    public static final Integer REFUND = 3;
    public static final Integer REFUND_FAILURE = 4;

    /**
     * 支付方式 1微信，2支付宝
     */
    public static final Integer WXPAY =1;
    public static final Integer ALIPAY =2;

    @TableId
    private String Id;

    private Long userId;

    //订单号
    private String number;

    //支付方式 1微信，2支付宝
    private Integer payMethod;

    //支付状态 0未支付 1已支付 2退款
    private Integer payStatus;

    //充值金额 分为单位
    private Integer rechargeAmount;

    //充值时间
    private LocalDateTime rechargeTime;

    //订单取消时间
    private LocalDateTime cancelTime;
}
