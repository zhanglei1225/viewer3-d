package io.renren.modules.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.pay.entity.TbRechargeRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * ClassName: RechargeMapper
 * Package: io.renren.modules.pay.mapper
 * Description:
 *
 * @Author Tree
 * @Create 2023/12/8 15:22
 * @Version 11
 */
@Mapper
public interface RechargeMapper extends BaseMapper<TbRechargeRecord> {

}
