package io.renren.modules.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.pay.entity.TbRechargeRecord;

import java.util.List;
import java.util.Map;

/**
 * ClassName: RechargeService
 * Package: io.renren.modules.pay.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/12/8 15:18
 * @Version 11
 */
public interface RechargeService extends IService<TbRechargeRecord> {
    Map<String, Object> nativePay(Integer rechargeAmount);

    /**
     * 查询订单列表，并倒序查询
     * @return
     */
    List<TbRechargeRecord> listOrderByCreateTimeDesc();

    /**
     * 支付成功，修改订单状态
     * @param outTradeNo
     */
    void paySuccess(String outTradeNo);


    /**
     * 取消订单
     * @param id
     */
    void userCancelById(Long id);

    /**
     * 退款成功，修改订单状态
     * @param outTradeNo
     */
    void refundSuccess(String outTradeNo);

    /**
     * 生成充值记录
     * @param rechargeAmount
     * @param payMethod
     * @return
     */
    TbRechargeRecord createOrderByAmount(Integer rechargeAmount, Integer payMethod);
}
