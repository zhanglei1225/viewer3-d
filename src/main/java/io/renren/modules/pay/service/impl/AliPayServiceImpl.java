package io.renren.modules.pay.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.AlipayConstants;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.renren.modules.pay.config.AliPayProperties;
import io.renren.modules.pay.entity.TbRechargeRecord;
import io.renren.modules.pay.service.AliPayService;
import io.renren.modules.pay.service.RechargeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;

/**
 * ClassName: AliPayServiceImpl
 * Package: io.renren.modules.pay.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/12/13 9:41
 * @Version 11
 */

@Service
@Slf4j
public class AliPayServiceImpl implements AliPayService {

    @Autowired
    private RechargeService rechargeService;

    @Resource
    private AlipayClient alipayClient;

    @Autowired
    private AliPayProperties aliPayProperties;

    /**
     * 统一收单下单并支付页面接口的调用
     * @param rechargeAmount
     * @return
     */
    @Transactional
    @Override
    public String tradeCreate(Integer rechargeAmount) {
        try {
            //生成订单
            log.info("生成订单");
            TbRechargeRecord record = rechargeService.createOrderByAmount(rechargeAmount, TbRechargeRecord.ALIPAY);
            //调用支付宝接口
            AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
            //配置需要的公共请求参数
            request.setNotifyUrl(aliPayProperties.getNotifyUrl());
            //支付完成后，我们想让页面跳转回前端页面，配置returnUrl
            request.setReturnUrl(aliPayProperties.getReturnUrl());
            //组装当前业务方法的请求参数
            JSONObject bizContent = new JSONObject();
            bizContent.put("out_trade_no", record.getNumber());
            BigDecimal total = new
                    BigDecimal(record.getRechargeAmount().toString()).divide(new BigDecimal("100"));
            bizContent.put("total_amount",total);
            bizContent.put("subject","支付宝支付");
            bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");
            request.setBizContent(bizContent.toString());
            //执行请求，调用支付宝接口
            AlipayTradePagePayResponse response =
                    alipayClient.pageExecute(request);
            if(response.isSuccess()){
                log.info("调用成功，返回结果 ===> " + response.getBody());
                return response.getBody();
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述===> " + response.getMsg());
                throw new RuntimeException("创建支付交易失败");
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("创建支付交易失败");
        }
    }

    /**
     * 支付通知
     * @param params
     * @return
     */
    @Override
    public String tradeNotify(Map<String, String> params) {
        String result = "failure";
        try {
            //异步通知验签
            boolean signVerified = AlipaySignature.rsaCheckV1(
                    params,
                    aliPayProperties.getPublicKey(),
                    AlipayConstants.CHARSET_UTF8,
                    AlipayConstants.SIGN_TYPE_RSA2); //调用SDK验证签名
            result ="success";
            if(!signVerified){
            //验签失败则记录异常日志，并在response中返回failure.
                log.error("支付成功异步通知验签失败！");
                return result;
            }
            // 验签成功后
            log.info("支付成功异步通知验签成功！");
            //按照支付结果异步通知中的描述，对支付结果中的业务内容进行二次校验，
            //1 商户需要验证该通知数据中的 out_trade_no 是否为商户系统中创建的订单号
            String outTradeNo = params.get("out_trade_no");
            TbRechargeRecord rechargeRecord = rechargeService.getOne(Wrappers.<TbRechargeRecord>lambdaQuery()
                    .eq(TbRechargeRecord::getNumber, outTradeNo));
            if(rechargeRecord == null){
                log.error("订单不存在");
                return result;
            }
            //2 判断 total_amount 是否确实为该订单的实际金额（即商户订单创建时的金额）
            String totalAmount = params.get("total_amount");
            int totalAmountInt = new BigDecimal(totalAmount).multiply(new
                    BigDecimal("100")).intValue();
            int totalFeeInt = rechargeRecord.getRechargeAmount();
            if(totalAmountInt != totalFeeInt){
                log.error("金额校验失败");
                return result;
            }
            //3 校验通知中的 seller_id（或者 seller_email) 是否为 out_trade_no 这笔单据的对应的操作方
            String sellerId = params.get("seller_id");
            String sellerIdProperty = aliPayProperties.getSellerId();
            if(!sellerId.equals(sellerIdProperty)){
                log.error("商家pid校验失败");
                return result;
            }
            //4 验证 app_id 是否为该商户本身
            String appId = params.get("app_id");
            String appIdProperty = aliPayProperties.getAppId();
            if(!appId.equals(appIdProperty)){
                log.error("appid校验失败");
                return result;
            }
            //在支付宝的业务通知中，只有交易通知状态为 TRADE_SUCCESS时，支付宝才会认定为买家付款成功。
            String tradeStatus = params.get("trade_status");
            if(!"TRADE_SUCCESS".equals(tradeStatus)){
                log.error("支付未成功");
                return result;
            }
            //处理业务 修改订单状态 记录支付日志
            processOrder(params,rechargeRecord);
            //校验成功后在response中返回success并继续商户自身业务处理，校验失败返回failure
            result = "success";
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 退款
     *
     * @param orderNo
     */
    @Override
    public void refund(String orderNo) {
        try {
            log.info("调用退款API");
            //调用统一收单交易退款接口
            AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
            //组装当前业务方法的请求参数
            TbRechargeRecord rechargeRecord = rechargeService.getOne(Wrappers.<TbRechargeRecord>lambdaQuery()
                    .eq(TbRechargeRecord::getNumber, orderNo));
            JSONObject bizContent = new JSONObject();
            bizContent.put("out_trade_no", orderNo);//订单编号
            BigDecimal refund = new BigDecimal(rechargeRecord.getRechargeAmount()).divide(new BigDecimal("100"));
            //BigDecimal refund = new BigDecimal("2").divide(new BigDecimal("100"));
            bizContent.put("refund_amount", refund);//退款金额：不能大于支付金额
            request.setBizContent(bizContent.toString());
            //执行请求，调用支付宝接口
            AlipayTradeRefundResponse response = alipayClient.execute(request);
            if(response.isSuccess()){
                log.info("调用成功，返回结果 ===> " + response.getBody());
            //更新订单状态
                rechargeService.update(Wrappers.<TbRechargeRecord>lambdaUpdate()
                        .set(TbRechargeRecord::getPayStatus,TbRechargeRecord.REFUND).eq(TbRechargeRecord::getNumber,orderNo));
            } else {
                log.info("调用失败，返回码 ===> " + response.getCode() + ", 返回描述 ===> "+ response.getMsg());
            //更新订单状态
                rechargeService.update(Wrappers.<TbRechargeRecord>lambdaUpdate()
                        .set(TbRechargeRecord::getPayStatus,TbRechargeRecord.REFUND_FAILURE).eq(TbRechargeRecord::getNumber,orderNo));
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            throw new RuntimeException("创建退款申请失败");
        }
    }

    /**
     * 处理订单
     * @param params
     */
    @Transactional(rollbackFor = Exception.class)
    public void processOrder(Map<String, String> params,TbRechargeRecord rechargeRecord) {
        log.info("处理订单");
        //处理重复通知
        //接口调用的幂等性：无论接口被调用多少次，以下业务执行一次
        Integer payStatus = rechargeRecord.getPayStatus();
        if (!TbRechargeRecord.UN_PAID.equals(payStatus)) {
            return ;
        }
        //获取订单号
        String orderNo = params.get("out_trade_no");
        //更新订单状态
        rechargeService.update(Wrappers.<TbRechargeRecord>lambdaUpdate()
                .set(TbRechargeRecord::getPayStatus,TbRechargeRecord.PAID).eq(TbRechargeRecord::getNumber,orderNo));
    }
}
