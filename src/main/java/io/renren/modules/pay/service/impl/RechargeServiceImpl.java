package io.renren.modules.pay.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.RedisUtils;
import io.renren.modules.pay.entity.TbRechargeRecord;
import io.renren.modules.pay.mapper.RechargeMapper;
import io.renren.modules.pay.service.RechargeService;
import io.renren.modules.pay.utils.WeChatPayUtil;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static io.renren.common.utils.ShiroUtils.getUserId;

/**
 * ClassName: RechargeServiceImpl
 * Package: io.renren.modules.pay.service.impl
 * Description:
 *
 * @Author Tree
 * @Create 2023/12/8 15:19
 * @Version 11
 */
@Service
public class RechargeServiceImpl extends ServiceImpl<RechargeMapper, TbRechargeRecord> implements RechargeService  {

    @Autowired
    private RechargeMapper rechargeMapper;
    @Autowired
    private WeChatPayUtil weChatPayUtil;
    @Autowired
    private RedisUtils redisUtils;

    private final String SPLIT = ":";

    @Override
    public Map<String, Object> nativePay(Integer rechargeAmount) {
        // 先从缓存中取值
        String obj = redisUtils.get(getUserId() + SPLIT + rechargeAmount);
        Map<String, Object> map1 = redisUtils.fromJson(obj, Map.class);
        if (map1!=null) {
            return map1;
        }
        // 生成充值记录
        TbRechargeRecord rechargeRecord = createOrderByAmount(rechargeAmount,TbRechargeRecord.WXPAY);
        // 调用native下单api
        String codeUrl = null;
        try {
             codeUrl = weChatPayUtil.getNative(rechargeRecord.getNumber(), BigDecimal.valueOf(rechargeRecord.getRechargeAmount()), "微信支付");
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        Map<String, Object> map = new HashMap<>();
        map.put("codeUrl", codeUrl);
        map.put("number", rechargeRecord.getNumber());
        // 缓存二维码，减轻频繁下单带来的服务器压力
        redisUtils.set(getUserId()+SPLIT+rechargeAmount,map,2*3600);
        return map;
    }

    /**
     * 查询订单列表，并倒序查询
     *
     * @return
     */
    @Override
    public List<TbRechargeRecord> listOrderByCreateTimeDesc() {
        List<TbRechargeRecord> tbRechargeRecords = rechargeMapper.selectList(Wrappers.<TbRechargeRecord>lambdaQuery().orderByDesc(TbRechargeRecord::getRechargeTime));
        return tbRechargeRecords;
    }

    /**
     * 支付成功，修改订单状态
     *
     * @param outTradeNo
     */
    @Override
    public void paySuccess(String outTradeNo) {
        // 当前登录用户id
        Long userId = getUserId();

        // 根据订单号查询当前用户的订单
        TbRechargeRecord tbRechargeRecord = rechargeMapper.selectOne(Wrappers.<TbRechargeRecord>lambdaQuery()
                .eq(TbRechargeRecord::getUserId, userId)
                .eq(TbRechargeRecord::getNumber, outTradeNo));

        // 根据订单id更新订单的支付状态、结账时间
        tbRechargeRecord.setPayStatus(TbRechargeRecord.PAID);
        tbRechargeRecord.setRechargeTime(LocalDateTime.now());
        rechargeMapper.updateById(tbRechargeRecord);
    }

    /**
     * 退款成功，修改订单状态
     *
     * @param outTradeNo
     */
    @Override
    public void refundSuccess(String outTradeNo) {
        // 当前登录用户id
        Long userId = getUserId();

        // 根据订单号查询当前用户的订单
        TbRechargeRecord tbRechargeRecord = rechargeMapper.selectOne(Wrappers.<TbRechargeRecord>lambdaQuery()
                .eq(TbRechargeRecord::getUserId, userId)
                .eq(TbRechargeRecord::getNumber, outTradeNo));

        // 根据订单id更新订单的支付状态、取消时间
        tbRechargeRecord.setPayStatus(TbRechargeRecord.REFUND);
        tbRechargeRecord.setCancelTime(LocalDateTime.now());
        rechargeMapper.updateById(tbRechargeRecord);
    }

    /**
     * 取消订单
     *
     * @param id
     */
    @Override
    public void userCancelById(Long id) {
        TbRechargeRecord tbRechargeRecord = rechargeMapper.selectById(id);
        //排除订单为空，状态未支付的情况，抛出异常
        if (tbRechargeRecord ==null) {
            throw new RuntimeException("订单不存在");
        }
        if (tbRechargeRecord.getPayStatus()==0) {
            throw new RuntimeException("订单未支付");
        }
        //给用户退款 修改为退款中
        try {
            weChatPayUtil.refund(tbRechargeRecord.getNumber(),tbRechargeRecord.getNumber(),
                    BigDecimal.valueOf(tbRechargeRecord.getRechargeAmount()),BigDecimal.valueOf(tbRechargeRecord.getRechargeAmount()));
            tbRechargeRecord.setPayStatus(TbRechargeRecord.REFUND_PROCESSING);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        //修改订单状态,取消订单时间
        tbRechargeRecord.setCancelTime(LocalDateTime.now());
        rechargeMapper.updateById(tbRechargeRecord);
    }

    /**
     * 生成充值记录
     * @param rechargeAmount
     * @return
     */
    public TbRechargeRecord createOrderByAmount(Integer rechargeAmount, Integer payMethod) {
        // 1.先查询是否有未支付的订单，进行删除
        LambdaQueryWrapper<TbRechargeRecord> wrapper = Wrappers.<TbRechargeRecord>lambdaQuery().eq(TbRechargeRecord::getPayStatus, TbRechargeRecord.UN_PAID)
                .eq(TbRechargeRecord::getUserId,getUserId());
        List<TbRechargeRecord> recordList = rechargeMapper.selectList(wrapper);
        if (recordList.size()>0) {
            List<String> idList = recordList.stream().map(TbRechargeRecord::getId).collect(Collectors.toList());
            rechargeMapper.deleteBatchIds(idList);
        }
        // 2.新增订单
        TbRechargeRecord rechargeRecord = new TbRechargeRecord();
        rechargeRecord.setRechargeTime(LocalDateTime.now());
        rechargeRecord.setRechargeAmount(rechargeAmount);
        rechargeRecord.setNumber(String.valueOf(System.currentTimeMillis()));
        rechargeRecord.setPayMethod(payMethod);
        rechargeRecord.setUserId(getUserId());
        rechargeMapper.insert(rechargeRecord);
        return rechargeRecord;
    }
}
