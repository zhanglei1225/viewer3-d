package io.renren.modules.pay.service;

import java.util.Map;

/**
 * ClassName: AliPayService
 * Package: io.renren.modules.pay.service
 * Description:
 *
 * @Author Tree
 * @Create 2023/12/13 9:40
 * @Version 11
 */
public interface AliPayService {

    String tradeCreate(Integer rechargeAmount);

    String tradeNotify(Map<String, String> params);

    /**
     * 退款
     * @param orderNo
     */
    void refund(String orderNo);
}
