package io.renren.modules.pay.controller;

import com.alipay.api.internal.util.AlipaySignature;
import io.renren.common.utils.R;
import io.renren.modules.pay.service.AliPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/pay/aliPay")
@Api(tags = "网站支付宝支付")
@Slf4j
public class AliPayController {
    @Resource
    private AliPayService aliPayService;

    @ApiOperation("统一收单下单并支付页面接口的调用")
    @PostMapping("/trade/page/pay/{rechargeAmount}")
    public R tradePagePay(@PathVariable Integer rechargeAmount) {
        log.info("统一收单下单并支付页面接口的调用");
        //支付宝开放平台接受 request 请求对象后
        // 会为开发者生成一个html 形式的 form表单，包含自动提交的脚本
        String formStr = aliPayService.tradeCreate(rechargeAmount);
        //我们将form表单字符串返回给前端程序，之后前端将会调用自动提交脚本，进行表单的提交
        //此时，表单会自动提交到action属性所指向的支付宝开放平台中，从而为用户展示一个支付页面
        return R.ok().put("formStr", formStr);
    }

    @ApiOperation("支付通知")
    @PostMapping("/trade/notify")
    public String tradeNotify(@RequestParam Map<String, String> params) {
        log.info("支付通知正在执行");
        log.info("通知参数 ===> {}", params);
        return aliPayService.tradeNotify(params);
    }

    /**
     * 申请退款
     * @param orderNo
     * @return
     */
    @ApiOperation("申请退款")
    @PostMapping("/trade/refund/{orderNo}")
    public R refunds(@PathVariable String orderNo){
        log.info("申请退款");
        aliPayService.refund(orderNo);
        return R.ok();
    }
}
