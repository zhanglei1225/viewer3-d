package io.renren.modules.pay.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.renren.common.utils.R;
import io.renren.modules.pay.entity.TbRechargeRecord;
import io.renren.modules.pay.service.RechargeService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WxPayController
 * Package: io.renren.modules.pay.controller
 * Description:
 *
 * @Author Tree
 * @Create 2023/12/8 15:10
 * @Version 11
 */
@Slf4j
@RestController
@RequestMapping("/pay/weXin")
public class WxPayController {

    @Autowired
    private RechargeService rechargeService;

    /**
     * 生成订单，返回二维码链接
     *
     * @param rechargeAmount
     * @return
     */
    @ApiOperation("生成订单，返回二维码链接")
    @GetMapping("getCodeUrl")
    public R getCodeUrl(Integer rechargeAmount) {
        Map<String, Object> map = rechargeService.nativePay(rechargeAmount);

        return R.ok(map);
    }

    @ApiOperation("订单列表")
    @GetMapping("/list")
    public R list(){
        List<TbRechargeRecord> list = rechargeService.listOrderByCreateTimeDesc();
        return R.ok().put("list", list);
    }

    /**
     * 查询本地订单状态
     */
    @ApiOperation("查询本地订单状态")
    @GetMapping("/query-order-status/{orderNo}")
    public R queryOrderStatus(@PathVariable String orderNo) {
        TbRechargeRecord rechargeRecord = rechargeService.getOne(Wrappers.<TbRechargeRecord>lambdaQuery().eq(TbRechargeRecord::getNumber, orderNo));
        Integer orderStatus = null;
        if (rechargeRecord !=null) {
            orderStatus =rechargeRecord.getPayStatus();
        }
        if (TbRechargeRecord.PAID.equals(orderStatus)) {//支付成功
            return R.ok("支付成功");
        }
        return R.error(101,"支付中...");
    }

    @PutMapping("/cancel/{id}")
    @ApiOperation("退款")
    public R userCancelById(@PathVariable Long id){
        log.info("退款,{}",id);
        rechargeService.userCancelById(id);
        return R.ok();
    }
}
