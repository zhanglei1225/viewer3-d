package io.renren.modules.pay.controller;

import com.alibaba.fastjson.JSONObject;
import com.sun.deploy.net.URLEncoder;
import io.renren.modules.pay.config.WeChatProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class WxAuthController {

    @Autowired
    private WeChatProperties weChatProperties;

    String appID = weChatProperties.getAppid();
    String mchID = weChatProperties.getMchid();
    String appSecret = weChatProperties.getSecret();
    String key = weChatProperties.getPrivateKeyFilePath();
    //申请授权码地址
    String wxOAuth2RequestUrl = "https://open.weixin.qq.com/connect/oauth2/authorize";
    //授权回调地址
    String wxOAuth2CodeReturnUrl = "http://xfc.nat300.top/transaction/wx‐oauth‐code‐return";
    String state = "";

    /**
     * 获取授权码
     * URLDecoder类包含一个decode(String s,String enc)静态方法，它可以将application/x-www-form-urlencoded MIME字符串转成普通字符串；
     * URLEncoder类包含一个encode(String s,String enc)静态方法，它可以将普通字符串转换成application/x-www-form-urlencoded MIME字符串。
     * @param httpRequest
     * @param httpResponse
     * @return
     * @throws IOException
     */

    @GetMapping("/getWXOAuth2Code")
    public String getWXOAuth2Code(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws IOException {
        /*https://open.weixin.qq.com/connect/oauth2/authorize?
        appid = APPID & redirect_uri = REDIRECT_URI & response_type = code & scope = SCOPE & state = STATE#wechat_redirect*/

        String url = String
                .format("%s?appid=%s&scope=snsapi_base&state=%s&redirect_uri=%s",
                        wxOAuth2RequestUrl, appID,
                        state, URLEncoder.encode(wxOAuth2CodeReturnUrl, "utf‐8"));
        return "redirect:" + url;
    }

    //授权通过跳转到 /wx‐oauth‐code‐return‐test?code=CODE&state=STATE
    @GetMapping("/wx‐oauth‐code‐return")
    public String wxOAuth2CodeReturn(@RequestParam String code, @RequestParam String state) {
    //https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
        //申请openid
        String tokenUrl = String
                .format("https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code",
                         appID, appSecret,code, "utf‐8");
        ResponseEntity<String> exchange = new RestTemplate().exchange(tokenUrl, HttpMethod.GET,
                null, String.class);
        String response = exchange.getBody();
        String openid= JSONObject.parseObject(response).getString("openid");
        //携带openid跳转至统一下单地址
        return "redirect:http://xfc.nat300.top/transaction/wxjspay?openid=" + openid;
    }

}
