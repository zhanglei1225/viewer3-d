package io.renren.modules.bim.service.impl;

import com.alibaba.fastjson.JSONObject;
import io.renren.common.utils.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.renren.modules.bim.dao.BimFileDao;
import io.renren.modules.bim.entity.BimFileEntity;
import io.renren.modules.bim.service.BimFileService;


@Service("bimFileService")
public class BimFileServiceImpl extends ServiceImpl<BimFileDao, BimFileEntity> implements BimFileService {
    public static Date accessTokenExpireDate; // accessToken凭证有效时间
    public static String myAccessToken; // accessToken保存凭证

    /**
     * 获取访问token
     * @return
     */
    public String getAccessToken() {
        // 如果凭证有效直接返回凭证
        Date currentDate = new Date();
        if (accessTokenExpireDate != null && currentDate.before(accessTokenExpireDate)) {
            return myAccessToken;
        }

        String accessToken = HttpClientUtils.httpsRequestPOST("https://api.bimface.com/oauth2/token", "");
        JSONObject tokenJson = JSONObject.parseObject(accessToken);
        JSONObject dataJson = tokenJson.getJSONObject("data");
        String expireTime = dataJson.getString("expireTime");
        myAccessToken = dataJson.getString("token");
        System.out.println("myAccessToken: " + myAccessToken);
        // 保存过期时间
        if (!StringUtils.isEmpty(expireTime)) {
            accessTokenExpireDate = DateUtils.parseDate(expireTime, DateUtils.DATE_TIME_PATTERN);
        }
        return myAccessToken;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String fileName = (String)params.get("key");
        String createUser = (String)params.get("createUser");

        IPage<BimFileEntity> page = this.page(
                new Query<BimFileEntity>().getPage(params),
                new QueryWrapper<BimFileEntity>()
                        .like(StringUtils.isNotBlank(fileName),"file_name", fileName)
                        .eq(createUser != null,"create_user", createUser)
                        .orderByDesc("create_date")
        );

        return new PageUtils(page);
    }

    @Override
    public void translate(Long fileId, String fileName) {
        // 获取accessToken
        String accessToken = getAccessToken();
        System.out.println(accessToken);
        FileTranslateRequest fileTranslateRequest = new FileTranslateRequest();
//        fileTranslateRequest.setCallback("http://localhost:8001/#/sys-log");
        fileTranslateRequest.setConfig(null);
        Source source = new Source();
        source.setCompressed(false);
        source.setFileId(fileId);
        source.setRootName(fileName);
        fileTranslateRequest.setSource(source);
        String jsonString = JSONObject.toJSONString(fileTranslateRequest);
        HttpClientUtils.httpsRequestPUT("https://api.bimface.com/translate",jsonString);
    }

    @Override
    public void getTranslateStatus() {
        getAccessToken();
        // 查询所有转换中的文件
        List<BimFileEntity> translateList = this.list(new QueryWrapper<BimFileEntity>().eq("status", 2));
        for (BimFileEntity translate : translateList) {
            String translateJson = HttpClientUtils.httpsRequestGET(null, "https://api.bimface.com/translate?fileId=" + translate.getFileId());
            JSONObject translateJsonObj = JSONObject.parseObject(translateJson);
            JSONObject dataJson = translateJsonObj.getJSONObject("data");
            String status = dataJson.getString("status");
            // 转换完成，更新数据库字段
            if ("success".equals(status)) {
                BimFileEntity bimFileEntity = new BimFileEntity();
                bimFileEntity.setStatus(1);
                this.update(bimFileEntity, new QueryWrapper<BimFileEntity>()
                        .eq("file_id", translate.getFileId())
                        .eq("status", 2));
            }
        }
    }
}