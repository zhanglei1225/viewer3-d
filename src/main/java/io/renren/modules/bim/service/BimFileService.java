package io.renren.modules.bim.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.bim.entity.BimFileEntity;

import java.util.Map;

/**
 * 文件上传
 *
 * @author cwx
 * @email cwx@gmail.com
 * @date 2021-03-02 10:44:46
 */
public interface BimFileService extends IService<BimFileEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void translate(Long fileId, String fileName);

    void getTranslateStatus();

    String getAccessToken();
}

