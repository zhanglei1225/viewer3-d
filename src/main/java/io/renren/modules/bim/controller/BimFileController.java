package io.renren.modules.bim.controller;

import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.bim.entity.BimFileEntity;
import io.renren.modules.bim.service.BimFileService;
import io.renren.modules.sys.controller.AbstractController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * 文件上传
 *
 * @author cwx
 * @email cwx@gmail.com
 * @date 2021-03-02 10:44:46
 */
@RestController
@RequestMapping("bim/bimfile")
public class BimFileController extends AbstractController {
    @Autowired
    private BimFileService bimFileService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("bim:bimfile:list")
    public R list(@RequestParam Map<String, Object> params){
        //只有超级管理员，才能查看所有数据
        if(getUserId() != Constant.SUPER_ADMIN){
            params.put("createUser", getUser().getUsername());
        }

        PageUtils page = bimFileService.queryPage(params);

        //  获取accessToken
        Map<String, Object> map =  new HashMap<>(1);
        map.put("accessToken", bimFileService.getAccessToken());
        return R.ok(map).put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("bim:bimfile:info")
    public R info(@PathVariable("id") Long id){
		BimFileEntity bimFile = bimFileService.getById(id);

        return R.ok().put("bimFile", bimFile);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("bim:bimfile:save")
    public R save(@RequestBody BimFileEntity bimFile){
		bimFileService.save(bimFile);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("bim:bimfile:update")
    public R update(@RequestBody BimFileEntity bimFile){
		bimFileService.updateById(bimFile);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("bim:bimfile:delete")
    public R delete(@RequestBody Long[] ids){
		bimFileService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    @RequiresPermissions("bim:bimfile:all")
    public R upload(@RequestBody BimFileEntity bimFile) throws Exception {
        //保存文件信息
        BimFileEntity bimFileEntity = new BimFileEntity();
        bimFileEntity.setFileName(bimFile.getFileName());
        bimFileEntity.setFileId(bimFile.getFileId());
        bimFileEntity.setCreateDate(new Date());
        bimFileEntity.setCreateUser(getUser().getUsername());
        bimFileService.save(bimFileEntity);

        return R.ok().put("url", "url");
    }

    /**
     * 上传文件
     */
    @PostMapping("/translate")
    @RequiresPermissions("bim:bimfile:all")
    public R translate(@RequestBody BimFileEntity bimFile) throws Exception {
        // 发起转换
        bimFileService.translate(Long.parseLong(bimFile.getFileId()), bimFile.getFileName());
        //保存文件信息
        BimFileEntity bimFileEntity = new BimFileEntity();
        bimFileEntity.setId(bimFile.getId());
        bimFileEntity.setStatus(2);
        bimFileService.updateById(bimFileEntity);

        return R.ok().put("url", "url");
    }

    /**
     * 查询转换任务是否结束
     */
    @PostMapping("/getTranslateStatus")
    @RequiresPermissions("bim:bimfile:all")
    public R getTranslateStatus() throws Exception {
        bimFileService.getTranslateStatus();

        return R.ok();
    }
}
