package io.renren.modules.bim.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 文件上传
 *
 * @author cwx
 * @email cwx@gmail.com
 * @date 2021-03-02 10:44:46
 */
@Data
@TableName("tb_bim_file")
public class BimFileEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long id;
	/**
	 * 上传文件id
	 */
	private String fileId;
	/**
	 * 上传文件名称
	 */
	private String fileName;
	/**
	 * 文件状态@0：已上传@1：转换完成@2：转换中文件状态@0：已上传@1：转换完成
	 */
	private Integer status;
	/**
	 * 删除状态@0：正常@1：删除
	 */
	@TableLogic
	private Integer isDelete;
	/**
	 * 上传用户名
	 */
	private String createUser;
	/**
	 * 创建时间
	 */
	private Date createDate;

	/**
	 * 过期时间
	 */
	private Date expireTime;
}
