package io.renren.modules.bim.dao;

import io.renren.modules.bim.entity.BimFileEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件上传
 * 
 * @author cwx
 * @email cwx@gmail.com
 * @date 2021-03-02 10:44:46
 */
@Mapper
public interface BimFileDao extends BaseMapper<BimFileEntity> {
	
}
