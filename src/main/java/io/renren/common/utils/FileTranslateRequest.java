package io.renren.common.utils;

import lombok.Data;

/**
 * @author cwx
 * @create 2021-03-04 4:15 下午
 */
@Data
public class FileTranslateRequest {
    private String callback;
    private String config;
    private Source source;
}
