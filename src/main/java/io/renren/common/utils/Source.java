package io.renren.common.utils;

import lombok.Data;

/**
 * @author cwx
 * @create 2021-03-04 4:17 下午
 */
@Data
public class Source {
    private Boolean compressed;
    private Long fileId;
    private String rootName;
}
